package testflight

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
)

type Requester struct {
	server *httptest.Server
}

var RedirectionError = errors.New("Redirection Error")

func (requester *Requester) Get(route string) *Response {
	return requester.performRequest("GET", route, map[string]string{}, map[string]string{})
}

func (requester *Requester) GetWithAuthorization(route, authorization string) *Response {
	headers := map[string]string{"Authorization": authorization}
	return requester.performRequest("GET", route, headers, map[string]string{})
}

func (requester *Requester) GetWithCookie(route, cookie string) *Response {
	headers := map[string]string{"Cookie": cookie}
	return requester.performRequest("GET", route, headers, map[string]string{})
}

func (requester *Requester) Post(route, contentType string, params map[string]string) *Response {
	headers := map[string]string{"Content-Type": contentType}
	return requester.performRequest("POST", route, headers, params)
}

func (requester *Requester) PostWithAuthorization(route, authorization, contentType string, params map[string]string) *Response {
	headers := map[string]string{"Authorization": authorization, "Content-Type": contentType}
	return requester.performRequest("POST", route, headers, params)
}

func (requester *Requester) Put(route, contentType string, params map[string]string) *Response {
	headers := map[string]string{"Content-Type": contentType}
	return requester.performRequest("PUT", route, headers, params)
}

func (requester *Requester) Delete(route, contentType string, params map[string]string) *Response {
	headers := map[string]string{"Content-Type": contentType}
	return requester.performRequest("DELETE", route, headers, params)
}

func (requester *Requester) Options(route string) *Response {
	return requester.performRequest("OPTIONS", route, map[string]string{}, map[string]string{})
}

func (requester *Requester) OptionsWithAccessControRequestHeaders(route, acrh string) *Response {
	headers := map[string]string{"Access-Control-Request-Headers": acrh}
	return requester.performRequest("OPTIONS", route, headers, map[string]string{})
}

func (requester *Requester) Do(request *http.Request) *Response {
	fullUrl, err := url.Parse(requester.httpUrl(request.URL.String()))
	if err != nil {
		panic(err)
	}

	request.URL = fullUrl
	return requester.sendRequest(request)
}

func (requester *Requester) Url(route string) string {
	return requester.server.Listener.Addr().String() + route
}

func (requester *Requester) performRequest(httpAction, route string, headers, params map[string]string) *Response {
	values := url.Values{}
	for k, v := range params {
		values.Set(k, v)
	}

	request, err := http.NewRequest(httpAction, requester.httpUrl(route), strings.NewReader(values.Encode()))
	if err != nil {
		panic(err)
	}

	for key, val := range headers {
		request.Header.Add(key, val)
	}

	return requester.sendRequest(request)
}

func (requester *Requester) sendRequest(request *http.Request) *Response {
	client := http.Client{
		CheckRedirect: checkRedirect,
	}

	response, err := client.Do(request)

	if err != nil {
		switch err.(type) {
		case *url.Error:
			if err.(*url.Error).Err == RedirectionError {
				return newResponse(response)
			}
		}
		panic(err)
	}

	return newResponse(response)
}

func (requester *Requester) httpUrl(route string) string {
	return "http://" + requester.Url(route)
}

// This function gets called before following the redirect
// on the following codes:
// 301 (Moved Permanently)
// 302 (Found)
// 303 (See Other)
// 307 (Temporary Redirect)
func checkRedirect(req *http.Request, via []*http.Request) error {
	// If we don't return _nil_ here, http.Client.Do() will return an *url.Error,
	// whose third field (``Err``) is the error we are specifying here:
	return RedirectionError
}
